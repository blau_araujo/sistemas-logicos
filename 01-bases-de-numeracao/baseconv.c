#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CHARS 100

/*
É preciso entender que os números passados na linha de comando
serão lidos como caracteres. Então, o número 3, por exemplo,
será o caractere '3' com valor inteiro 51.

Para ter o inteiro 3, então, nós precisamos subtrair seu valor
(51) do valor do caractere '0' (48): 51-48 = 3

O mesmo vale para dígitos alfabéticos. Para ter o inteiro 11
como valor do caractere 'B', precisamos subtrair seu valor
(66) do valor do caractere 'A' (65) e somar 10: (66-65)+10=11
*/
int digito(char d) {
    if (d >= '0' && d <= '9') {
        return d - '0';
    } else if (d >= 'A' && d <= 'F') {
        return d - 'A' + 10;
    } else if (d >= 'a' && d <= 'f') {
        return d - 'a' + 10;
    } else {
        puts("Número inválido! A maior base permitida é 16!");
        exit(1);
    }
}

/*
No processo inverso, um inteiro precisa ser convertido em caractere,
o que pode ser feito somando o inteiro ao valor do caractere.
*/
char digchar(int d) {
    char dc;
    if (d >= 0 && d <= 9) {
        dc = d + '0';
    } else {
        dc = d - 10 + 'A';
    }
    return dc;
}

/*
Para converter um número de uma base para outra, é preciso
sempre converte-lo para a base 10 primeiro, o que é feito
pela soma da multiplicação dos dígitos pelas potências da base.
*/
int base_to_dec(char *number, int base) {

    int number_len = strlen(number);
    int exponent   = 1;
    int decimal    = 0;
    int i;

    // Vamos ler os dígitos da direita para a esquerda...
    for (i = number_len - 1; i >= 0; i--) {

        // O valor do dígito não pode ser maior do que 
        // o número da base de origem...
        if (digito(number[i]) >= base) {
            puts("Número inválido na base informada!");
            exit(2);
        }

        decimal += digito(number[i]) * exponent;
        exponent = exponent * base;
    }

    return decimal;

}

/*
A conversão da base 10 para outra base é feita com
divisões sucessivas do número pela base e guardando
os restos.
*/
char* dec_to_base(int number, int base, char *conv) {

    char tmp;
    int len;
    int i = 0;

    do {
        conv[i++] = digchar(number % base);
        number /= base;
    } while (number > 0);
    conv[i] = '\0';

    // Invertendo a string...
    len = strlen(conv);
    for (i = 0; i < len / 2; i++) {
        tmp = conv[i];
        conv[i] = conv[len - i - 1];
        conv[len - i -1] = tmp;
    }

    return conv;

}

int main(int argc, char **argv) {

    int base_orig;
    int base_dest;
    int number_dec;
    char conv[MAX_CHARS];

    // Os parâmetros são obrigatórios...
    if (argc < 3) {
        puts("Número incorreto de parâmetros!");
        puts("Uso: baseconv NÚMERO BASE_ORIGINAL [BASE_DESTINO]");
        exit(3);
    }

    base_orig  = atoi(argv[2]);
    number_dec = base_to_dec(argv[1], base_orig);

    // Se BASE_DESTINO não for informada, a conversão para
    // na base 10, caso contrário, uma nova conversão é feita.
    if (argc < 4) {
        printf("%d\n", number_dec);
    } else {
        base_dest = atoi(argv[3]);
        if (base_dest < 2 || base_dest > 16) {
            puts("\nBase de destino inválida!\n");
            exit(4);
        }
        puts(dec_to_base(number_dec, base_dest, conv));
    }


    return 0;
}
