# 1 - Bases de numeração: programas

## baseconv.c

### Compilação

```
:~$ gcc -Wall baseconv.c -o baseconv
```

### Descrição

Converte um número de uma base original para outra.

### Uso

```
:~$ baseconv NÚMERO BASE_ORIGEM [BASE_DESTINO]
```

Se `BASE_DESTINO` não for informada, a conversão será feita para a base 10.

### Exemplo

Convertendo 235 na base 16 para a base 8...

```
:~$ baseconv 235 16 8
1065
```

Convertendo 235 na base 16 para a base 10...

```
$ baseconv 235 16
565
```


## fbaseconv.c

### Compilação

```
:~$ gcc -Wall fbaseconv.c -o fbaseconv
```

### Descrição

Converte um número de uma base original para outra e exibe a saída formatada.

### Uso

```
:~$ baseconv NÚMERO BASE_ORIGEM [BASE_DESTINO]
```

Se `BASE_DESTINO` não for informada, a conversão será feita para a base 10.

### Exemplo

Convertendo 235 na base 16 para a base 8...

```
:~$ fbaseconv 235 16 8
(235)16 (565)10 (1065)8
```

Convertendo 235 na base 16 para a base 10...

```
$ fbaseconv 235 16
(235)16 (565)10
```



